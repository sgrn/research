% Emergence (c) by Sangsoic
%
% Emergence is licensed under a
% Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-nc-sa/3.0/>.
\documentclass[a4paper]{report}

% include package
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{multirow}
\usepackage{fancyhdr}
\usepackage{hhline}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage[margin=0.7in]{geometry}
\usepackage{lastpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{titlesec}
\usepackage{ragged2e}
\usepackage[table]{xcolor}
\usepackage{etoolbox}
\AtBeginEnvironment{quote}{\singlespacing\large}

% changing title format
\titleformat{\chapter}[display]{\normalfont\bfseries}{}{0pt}{\LARGE}

% Redefine the plain page style
\fancypagestyle{plain}{
	\fancyhf{}
	\fancyhead[LE,RO]{\slshape \rightmark}
	\fancyhead[LO,RE]{\slshape \leftmark}
	\fancyfoot[C]{\thepage\ sur \pageref{LastPage}}
	\renewcommand{\headrulewidth}{0.4pt} % Line at the header visible
	\renewcommand{\footrulewidth}{0.4pt} % Line at the footer visible
}
\pagestyle{plain}

% \renewcommand{\thesection}{\arabic{section}}

% first page information
\title{Emergence}
\author{\scshape Sangsoic} 
\date{Jun 22 2020}

\begin{document}

	% display first page information
	\maketitle
	\clearpage

	% display table of contents
	\tableofcontents

	\clearpage

	\chapter{Introduction}

		\section{Characterization of the emergence phenomenon} \label{definition}
			{\large \textit{Emergence} is a natural phenomenon, which allows complexity to arise from simplicity. It describes the universal process of creation, in which interesting properties and features emerge as soon as we place interacting elementary parts together. Likewise, this highly abstract concept can be observed at all scales across the universe. From atoms organizing into chemical compounds, to the formation of ant colonies, or even the structuring of human societies.\\
			As emergence has been showing up everywhere since the dawn of time, it was repeatedly studied by philosophers, which considered it as a misconceived and mysterious event. Nonetheless, alongside inventions such as complexity theory, and system theory, we are now progressively acquiring the necessary mathematical and computational tools in order to comprehend it in a more logical, and rigorous scientific manner.}

		\begin{figure}[h!]
			\centering
			\begin{subfigure}[b]{0.4\linewidth}
				\includegraphics[width=\linewidth]{Img/civilization.jpg}
				\caption{Human civilization.}
			\end{subfigure}
			\begin{subfigure}[b]{0.4\linewidth}
				\includegraphics[width=\linewidth]{Img/ants.jpg}
				\caption{Ant colonies.}
			\end{subfigure}
			\begin{subfigure}[b]{0.4\linewidth}
				\includegraphics[width=\linewidth]{Img/atoms.jpg}
				\caption{Oxygen atoms.}
			\end{subfigure}
			\caption{Examples of systems inhibiting emergence properties.}
			\label{fig:introexample}
		\end{figure}

		\clearpage

		\section{Subject and problematic} \label{problematic}
			{\large In this project I am planning to build two computer programs. A first one, capable of simulating and producing variable emergence phenomena, which will then be processed and analyzed by a second program, in order to subsequently tweak the input parameters of the simulation. I expect via this dynamic model, to experimentally and empirically answer to the following problematic.}
			\begin{center}
			\textit{\Large ``How to maximize the occurrence of emergence phenomena ?''}
			\end{center}
			{\large To be able to answer this question properly I will have to research and study topics, such as :}
			\begin{itemize}
				\item Pattern formation.
				\item Synergies and interactions.
				\item Weak and Strong emergence.
				\item Integrative levels.
				\item Micro and Macro Dynamics.
				\item Chaos and order.
				\label{emergenencetopics}
			\end{itemize}
			{\large Furthermore, I chose to center the simulation program capabilities around two-dimensional cellular automata, as they can produce a wide range of interesting and varied results, whilst being feasible in terms required resources. Therefore, matters such as :}
			\begin{itemize}
				\item Antomata theory.
				\item Abstract machines.
				\item Complexity theory and organization.
			\end{itemize}
			{\large will be explored.}

		\section{Tasks distribution and description} \label{task}

			{\large As mentioned in the \ref{problematic} section, this project will be divided into two mains parts which will interact with one another. Hence, creating a dynamic system capable of adjusting itself, gradually moving towards the expected result. Here is a graphical representation of the system.}

			\begin{figure}[h!]
				\centering
				\includegraphics[scale=0.5]{Img/global_diagram.png}
				\caption{Global diagram of the system.}
				\label{fig:globalsystem}
			\end{figure}

			\clearpage

			\subsection{Quick description}

				\subsubsection{Simulation}

					{\large The output of the simulation will consist of a two-dimensional grid of cells. Each cell will have a state, which can vary through time, depending on the rules given by the ML script. The first step is to generate the initial grid layout and load up the sequence of rules from an input configuration file. Then, the program must walk through the entire grid, updating the cells if necessary. This process is referred as an iteration and it is repeated a variable number of times. After each iteration, a snapshot of the entire grid is stored chronologically. Finally, as soon as there is no more iterations left to do, the simulation transfers the snapshots to the ML script for analysis.}

				\subsubsection{IO exchange formats}
					{\large The system has two ways of transferring data from one part to the other. Therefore, I need to define a text configuration format in which the simulation will interpret the input layouts, as well as a binary format in which output snapshots will be encoded.}

				\subsubsection{ML script}
					{\large This part of the project is dedicated to creating the user interface for the ML script program along with a scripting language which will allow the user to edit the simulation input configuration files or the ML algorithm to generate them accordingly. The ML algorithm must generate input configuration files from which will result emergent properties. Hence, this part of the project will contain machine learning, as well as a genetic algorithm.}

	\chapter{Research}

		{\large This chapter will cover the topics listed at the \ref{emergenencetopics} section. I researched these topics in order to better understand and characterize all the processes behind emergence phenomena. Indeed, comprehending the principles behind these concepts is essential if our ultimate goal is to maximize the appearance of emergence events within our system.}

		\section{Patterns}
			{\large Just as emergence, the word pattern describes an highly abstract notion, and it is used in many fields. From art, in which it is understood as an aesthetic concept, to natural science such as computing, physics or even biology where it is studied in a more structural and rigorously defined way. Furthermore, patterns being extremely theoretical and abstract, it makes the task of defining what they are a really hard and laborious. Yet, after a lot of research, it seems that patterns can be qualified as any form of correlation between the states of elements within a system. In our case, the system will be the cellular automaton simulation, and the elements will be the cells composing the two-dimensional grid.}

			\begin{figure}[h!]
				\centering
				\begin{subfigure}[b]{0.4\linewidth}
					\includegraphics[width=\linewidth]{Img/turbulence_of_Ginzburg-Landau_Equation.jpeg}
					\caption{Turbulence of Ginzburg-Landau Equation.}
				\end{subfigure}
				\begin{subfigure}[b]{0.4\linewidth}
					\includegraphics[width=\linewidth]{Img/dynamic_networks.jpeg}
					\caption{Dynamic networks.}
				\end{subfigure}
				\caption{Examples of patterns.}
				\label{fig:patterns}
			\end{figure}

			{\large To conclude, because patterns are everywhere, they can be the direct product of emergence processes. Hence, pattern analysis turns out to be a great tool for the detection of emergence phenomena. With that, we should observe two types of pattern formation during the analysis of the simulation output, both spatial patterns, happening in space between agents, and also temporal patterns, meaning it will occur over time, in our case over a certain number of iterations.}

		\clearpage

		\section{Synergies}

			{\large Synergies are one of the essential building blocks that constitute emergence. A synergy is specific kind of interaction between several elements. This interaction must produce an effect and possess new characteristics which are greater or lesser than the ones of its isolated constituents.\\
			An example of synergies in real world application would be the way ants work together in a colony to achieve something bigger than what one sole ant could ever achieve alone. Another familiar example would be the structuring of human companies, whereby the interaction of all employees makes the whole enterprise produce something far more complex than an isolated worker.}

			\begin{figure}[h!]
				\centering
				\begin{subfigure}[b]{0.4\linewidth}
					\includegraphics[width=\linewidth]{Img/ant_synergy.jpg}
					\caption{Ant workers creating a bridge for other ants to pass through.}
				\end{subfigure}
				\begin{subfigure}[b]{0.4\linewidth}
					\includegraphics[width=\linewidth]{Img/wallstreet_trading_floor.jpeg}
					\caption{Traders on a Wall Street trading floor}
				\end{subfigure}
				\caption{Examples of synergistic interactions.}
				\label{fig:synergies}
			\end{figure}

			{\large Synergies can be perceived as form of non-linear interactions which means these interactions somehow bring or take of value from the system, above that of the individual components. This is in contrast to linear interactions, where the behavior of the entire system can be reduced and predicted by the behavior of its constituents. Lastly, now that we are familiar with what synergies are, it is critical that we observe synergistic interactions in between the elements of our non-linear simulation.}

		\section{Weak and Strong Emergence}

			{\large We can observe two kinds of emergence phenomena in nature, strong and weak. Both types are necessary, but it is important to keep them separate. This section aim to express their differences and particularities.} 

			\subsection{Weak}
				{\large An emergence phenomenon is qualified as weak if the roles and functions played by its parts are fully determined. Furthermore, a system inhibiting weak emergence properties do not exist independently from its constituents. Thus, it is simple to predict the outcome of weak emergence phenomena given that we possess the required computing power. Ultimately, such systems can easily be reduced to the functions of its interacting elements. Then, we can derive the system's behavior at higher levels, only by knowing how the lower levels work. An example of weak emergent systems would be Conway's game of life.}

			\begin{figure}[h!]
				\centering
				\begin{subfigure}[b]{0.4\linewidth}
					\includegraphics[width=\linewidth]{Img/conway0.jpg}
					\caption{A first example of Conway's game of life.}
				\end{subfigure}
				\begin{subfigure}[b]{0.4\linewidth}
					\includegraphics[width=\linewidth]{Img/conway1.jpg}
					\caption{A second example of Conway's game of life.}
				\end{subfigure}
				\caption{Examples of weak emergence}
				\label{fig:weak}
			\end{figure}

			\subsection{Strong}
				{\large Strong emergence differ from weak emergence in its ability to be hardly predictable as well as not entirely reducible to its lower level constituents. Indeed, systems inhibiting such properties must exist independently from the functionalities of their parts along with somehow having an unexpected behavior when analyzing the elementary components. The idea of strong emergence embrace the fact that something truly new has been created on the higher level of the system. Indeed, rules on a certain level cannot be derived by simpler rules acting at lower levels inside the system. A real world application of this principle could be the concept of quantum entanglement, where the state of two quantum particles are entirely dependent upon each other.}

			\begin{figure}[h!]
				\centering
				\begin{subfigure}[b]{0.4\linewidth}
					\includegraphics[width=\linewidth]{Img/quantum_entanglement.jpg}
					\caption{A first ever taken photo of Quantum entanglement.} 
				\end{subfigure}
				\begin{subfigure}[b]{0.4\linewidth}
					\includegraphics[width=\linewidth]{Img/qe1.jpg}
				\end{subfigure}
				\caption{Examples of strong emergence}
				\label{fig:strong}
			\end{figure}

		\section{Integrative levels} \label{integrativelevels}
			{\large Integrative levels can be seen as the direct product of emergent processes inasmuch as they correspond to a set of organized phenomena emerging from other pre-existing agents, interacting at a lower-level. This intellectual framework is used to scale, quantify, and classify emergence phenomena in a hierarchical manner. Indeed, we can often observe the product of multi-layer emergence in reality, as physical phenomenon vary in function of the scale studied. Naturally, we know that physics is different than biology, which is also different than sociology and so on... This is true because the rules guiding the behavior of elements at a certain scale, are not necessarily the same that the ones governing elements at another scale. For this reason, integrative levels are valuable tool, specially if we want to be able to describe and classify what will happen when we observe the output of our simulation.}

			\begin{figure}[h!]
				\centering
				\includegraphics[scale=0.2]{Img/mapofphysics.jpg}
				\caption{Map of physics}
				\label{fig:intlevels}
			\end{figure}

		\section{Micro-Macro dynamics}
			{\large As explained the \ref{integrativelevels} section, emergence phenomenon can encapsulate a vast number of sub levels called integrative levels. Each level possesses its own rules and is dependent on the previous level. Nevertheless, there is two special levels called micro-level and macro-level which consist respectively of the lowest and highest level of any emergent phenomenon.}

			\subsection{Micro-level}
				{\large The micro-level is the lowest level of any kind of system. Components at this level interact directly with the physical world in which the phenomenon occurs. Moreover, despite the fact that each constituent is elementary, yet their interactions is completely responsible for the behavior of the greater whole from which they are part of.}

			\begin{figure}[h!]
				\centering
				\includegraphics[scale=0.2]{Img/gameoflifemicrolevel.jpg}
				\caption{Game of life at a micro-level}
				\label{fig:micro}
			\end{figure}

			\subsection{Macro-level}
				{\large The macro-level is the highest level of any kind of a system. Components at this level depend on a lot of variables and because they are themselves composed of many other parts, they cannot be considered as elementary. Besides, the macro-level acts like an interface between the environment in which the system is evolving, and all the other sub levels of the system. This is how lower levels such as the micro-level are also dependent on the macro-level.}

			\begin{figure}[h!]
				\centering
				\includegraphics[scale=0.2]{Img/gameoflifesimulatingitself.jpg}
				\caption{Game of life at a macro-level, simulating itself}
				\label{fig:macro}
			\end{figure}

			{\large Conclusively, the micro-macro dynamic represents how these two levels influence and depend upon each other.}

		\section{Chaos and order}

			{\large The perpetual duality between order and chaos in a system, is a notion I chose to explore because the goal is to determine the correct set of rules and initial configurations in order to produce emergence phenomena. That being so, it is crucial for us, to understand when a given system state switches from being organized to disorganized, or reciprocally, going from being disorganized to organized.\\ \\
			This notion of phase transition is called the edge of chaos and is hypothesized to occur within plenty of varied systems. Fortunately for us, cellular automata were one of the first systems studied by \href{https://en.wikipedia.org/wiki/Christopher_Langton}{Christopher Langton} that lead to the discovery of this theory.\\ \\
			Indeed, during his research, Langton defined a quantity for all cellular automata named lambda \( \lambda \), which corresponds to the level of change taking place for a particular set of rules and initial configurations. Thereby, low values of \( \lambda \) resulted in less change happening among the cells, while bigger values of \( \lambda \) produced an outcome richer in alterations. Then, he proved that cellular automata with the most emergent properties where those with a balanced level of \( \lambda \), as systems with the bigger or lesser \( \lambda \) values were either prone to complete randomness or too much stability for emergence to arise.}

			\begin{figure}[h!]
				\centering
				\begin{subfigure}[b]{0.4\linewidth}
					\includegraphics[width=\linewidth]{Img/chaostheory.png}
					\caption{A plot of the Lorenz attractor for values.} 
				\end{subfigure}
				\begin{subfigure}[b]{0.4\linewidth}
					\includegraphics[width=\linewidth]{Img/selforga.jpeg}
					\caption{Birds grouping} 
				\end{subfigure}
				\caption{Examples of strong emergence}
				\label{fig:chaosorder}
			\end{figure}

	\chapter{Simulation}

		\section{Concepts}

			{\large The simulation will consist of a two-dimensional discrete cellular automata. Despite the fact that the elementary rules are yet to be determined during analysis by the ML script, I already have few general ideas on what concepts would be judicious to implement, if our goal is to make the simulation as tweakable as possible.}

			\subsection{Field Cells}

				{\large A field cell would be a cell which stores information that influence the displacement of other cells such as magnitude and direction. Hence, their quantity, origins as well as the magnitude, and the direction that they contain could depend on instructions given by the ML script.}

			\begin{figure}[h!]
				\centering
				\includegraphics[scale=0.2]{Img/vectorfield.jpeg}
				\caption{Vector field.}
				\label{fig:vectorfield}
			\end{figure}

			\subsection{Agent Cells}

				{\large An agent cell would be a cell whose only purpose is to obey to instructions contained within field cells. We could think of them as the actual cells displayed on the screen and their motions and arrangements would be responsible for pattern formations. Again, the way that they interact and follow the instructions contained within field cells, could be defined completely by the ML script.\\}
		\section {Adaptability}

			{\large These two notions are great, as they could be used as foundations, without really limiting the ways the simulation could be configured and initialized. Indeed, I can think of several approaches in order to change the entire simulation's behavior. For instance, the modification of the sequences responsible for the generation of the magnitude of each field cell or even changing the expression of the function which outputs the direction for the next move of a given agent cell.}

\end{document}
