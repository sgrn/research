% Euclid's Algorithm (c) by Sangsoic
%
% Euclid's Algorithm is licensed under a
% Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-nc-sa/3.0/>.

\documentclass[a4paper]{report}

% include package
\usepackage[utf8]{inputenc}
\usepackage[
	type={CC},
	modifier={by-nc-sa},
	version={3.0},
]{doclicense}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{multirow}
\usepackage{fancyhdr}
\usepackage{hhline}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage[margin=0.7in]{geometry}
\usepackage{lastpage}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{titlesec}
\usepackage{ragged2e}
\usepackage[table]{xcolor}
\usepackage{etoolbox}
\usepackage[linesnumbered,lined,boxed,commentsnumbered]{algorithm2e}
\AtBeginEnvironment{quote}{\singlespacing\large}

% changing title format
\titleformat{\chapter}[display]{\normalfont\bfseries}{}{0pt}{\LARGE}

% Redefine the plain page style
\fancypagestyle{plain}{
	\fancyhf{}
	\fancyhead[LE,RO]{\slshape \rightmark}
	\fancyhead[LO,RE]{\slshape \leftmark}
	\fancyfoot[C]{\thepage\ sur \pageref{LastPage}}
	\renewcommand{\headrulewidth}{0.4pt} % Line at the header visible
	\renewcommand{\footrulewidth}{0.4pt} % Line at the footer visible
}
\pagestyle{plain}

% \renewcommand{\thesection}{\arabic{section}}

% first page information
\title{Euclid's Algorithm}
\author{\scshape Sangsoic} 
\date{September 10 2020}


\begin{document}

	% display first page information
	\maketitle
	\clearpage

	% display table of contents
	\tableofcontents

	\clearpage

	\chapter{Euclid's Algorithm}

		\section{Introduction} 

		{\large If you read this, you are probably looking for to understand why Euclid's algorithm works. Some teacher might have told you what it is, what it does, but hasn't bothered explaining why that is. In this short course, I'm going to demonstrate why it works, starting with a simple, non rigorous, intuitive and graphic explanation and then, for those who are interested in going further, I will prove that it works for all cases in a more rigorous mathematical way.}

		\begin{figure}[h!]
			\centering
			\includegraphics[scale=0.25]{Img/intro.png}
		\end{figure}

		\clearpage

		\section {How does it work} \label{section:howdoesitwork}

			\begin{large}

			First of all, without even talking about Euclid's Algorithm, let's take a simple case of GCD. For instance $gcd(10, 4)$ :
			\\
			\begin{itemize}
				\item Because the GCD needs to divide both $10$ and $4$ we can right away deduce that \\ 
				$gcd(10, 4) \leq 4$ as any number $x \in \mathbb{Z} \backslash \lbrace 0 \rbrace$ such that $\lvert x \rvert > 4$ \textit{cannot divide it} \footnote{Let $(a, b) \in \mathbb{Z} \backslash \lbrace 0 \rbrace$, such that $a < b$. If $0 < \lvert a \rvert < \lvert b \rvert$, then $-1 > \frac{a}{b} > 0$ or $0 < \frac{a}{b} < 1$.}
				\item Next we can restrict our search for the GCD to only positive integer (in $\mathbb{N}$) as if a number $b$ such that $b \in \mathbb{Z}^{-},  b\ |\ a$, we know that its additive inverse $-b\ |\ a$.
				\item Finally as the division by $0$ is not determined, $0$ cannot divide any number which means it cannot be the GCD of any pair of numbers.
			\end{itemize}
			
			Therefore $1 \leq gcd(10, 4) \leq 4$. At this point, the intuitive human thing to do would be to list the divisors of $4$ and then take the largest that also divides $10$. In this case it happens to be 2. We can then confidently state that the $gcd(10, 4) = 2$. However, this method is far from being the best way of doing so, specially if you count on programming it on a computer. \\
			If we now take Euclid's approach into consideration (which will be defined formally in the second part of this course \ref{section:rigorous}), we can calculate the GCD in the following way: 

			\begin{equation}
				\begin{split}
					gcd(10, 4) & = gcd(4, 2), \ \text{as}\ 10 = 4 \times 2 + 2\\
					& = gcd(2, 0), \ \text{as}\ 4 = 2 \times 1 + 0 \\
					& = 2
				\end{split}
			\end{equation}

			As any integer (apart from $0$ itself) can divide 0. Hence, the GCD between any non null integer and $0$ will always result in that same number (ie) $\forall a \in \mathbb{Z}^*, a\ |\ 0$.\\

			Here is another example to finish this section.

			\begin{equation}
				\begin{split}
					gcd(23, 16) & = gcd(16, 7), \ \text{as}\ 23 = 16 \times 1 + 7\\
					& = gcd(7, 2), \ \text{as}\ 16 = 7 \times 2 + 2 \\
					& = gcd(2, 1), \ \text{as}\ 7 = 2 \times 3 + 1 \\
					& = gcd(1, 0), \ \text{as}\ 2 = 1 \times 2 + 0 \\
					& = 1
				\end{split}
			\end{equation}

			The $gcd(23, 7) = 1$.

			\end{large}

		\clearpage

		\section{Intuitive explanation of why it works}

			\begin{large}
				Let's take another example to illustrate Euclid's algorithm process graphically. For instance the $gcd(59, 24)$ :\\

					Firstly, we want to decompose $59$ in segments of length $24$, plus an additional rest segment if $24$ does not perfectly divide $59$. After calculating the euclidean division of $59$ by $24$, we find that $59$ can be expressed as $2$ segments of length $24$ plus a rest segment of length $11$.\\
					

					\begin{figure}[h!]
						\centering
						\includegraphics[scale=0.15]{Img/par1.png}
					\end{figure}


					Using this representation, finding out the $gcd(59,24)$ is just like finding the longest segment that divides both a segment of length $59$ and a segment of length $24$.\\

					\begin{figure}[h!]
						\centering
						\includegraphics[scale=0.15]{Img/par2.png}
					\end{figure}

					One pretty useful property, is that any segments of length $B$, capable of dividing a segment of length $A$, will also be able to divide any segments of length multiple of $A$.\\

					\begin{figure}[h!]
						\centering
						\includegraphics[scale=0.15]{Img/par3.png}
					\end{figure}

					\begin{figure}[h!]
						\centering
						\includegraphics[scale=0.15]{Img/par3_sec.png}
					\end{figure}

					\clearpage

					However, we can now simplify the problem by only finding out the $gcd(24, 11)$ as we were able to represent $59$ with $2$ segments of length $24$ and a segment of length $11$.\\


					We have to repeat this process until our rest segment have a length of $0$. Once we have attained this last step, we know that the GCD between any non-null integer and 0 is the non-null integer itself.\\

					\begin{figure}[h!]
						\centering
						\includegraphics[scale=0.15]{Img/par4.png}
					\end{figure}

					The final result is $1$, which means that the longest segment that divides both a segment of length $59$ and a segment of length $24$ is a segment of length $1$.\\

			The modulus method is faster than the subtraction method, because it utilizes the division instead of the subtraction to decompose numbers. Here is a step by step comparison between the modulus and subtraction method :
					\begin{figure}[h!]
						\centering
						\includegraphics[scale=0.15]{Img/par5.png}
					\end{figure}

					\begin{figure}[h!]
						\centering
						\includegraphics[scale=0.15]{Img/par4.png}
					\end{figure}
			\end{large}


		\clearpage

		\section{Rigorous Explanation} \label{section:rigorous}

			{\large For those who are interested in a rigorous proof let's dive into the mathematics of it.}

			\subsection {Theorem}

				\begin{large}

				First of all, it is important to mention that Euclid's Algorithm is based on : \\

				\begin{itemize}
					\item The following theorem.

					\[\forall (a, b) \in \mathbb{Z}^* \times \mathbb{Z}^*, gcd(a,b) = gcd(b,r) \ \text{with}\  a = b \times q + r , (q, r) \in \mathbb{Z}^2\]

					\item The following fact.

					\[\forall a \in \mathbb{Z} \backslash \lbrace 0 \rbrace , gcd(a,0) = gcd(0,a) = a\] \\
				\end{itemize}

				From these two assertions we can clearly guess that the algorithmic definition of the GCD function will be recursive, as we now have both the \textit{recursive case} \footnote{The case in witch the definition the object comprise its own self.} and \textit{base case} \footnote{The case in which the definition of the object is not recursive, it allows the recursion process to stop.}.

				\end{large}

			\subsection{Algorithmic Definition}

				{\large We are ready to use Euclid's algorithm to define the gcd function recursively.}

				\IncMargin{1em}
				\begin{algorithm}[H]
					\SetAlgoLined
					\DontPrintSemicolon
					\SetKwData{Left}{left}\SetKwData{This}{this}\SetKwData{Up}{up}
					\SetKwFunction{Union}{Union}\SetKwFunction{FindCompress}{FindCompress}
					\SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
					\Input{$(a, b) \in \mathbb{Z}^2 , a \neq 0 \lor b \neq 0$}
					\Output{Greatest Common Divisor of a and b.}
					\SetKwFunction{FMain}{gcd}
					\SetKwProg{Fn}{Function}{:}{}
					\Fn{\FMain{$a, b$}}{
						\If{$b = 0$}{
							\textbf{return} $a$ \;
						}
						\textbf{return }\textit{gcd}(b,$a\ mod\ b$) \;
					}
					\caption{Greatest Common Divisor}\label{algo}
				\end{algorithm}
				\DecMargin{1em}

			\clearpage

			\subsection{Proof}

				\
				\begin{large}
					Now, we have to prove the theorem mentioned above \ref{section:rigorous} in order to prove that Euclid's algorithm works. So let's do it !!. \\

					\noindent We want to show that $\forall (a, b) \in \mathbb{Z}^* \times \mathbb{Z}^*, gcd(a, b) = gcd(b, r)$ with $a = bq + r, (q, r) \in \mathbb{Z}^2$. \\

					Let :
					\begin{itemize}
						\item $(a, b, q, r) \in \mathbb{Z}^* \times \mathbb{Z}^* \times \mathbb{Z} \times \mathbb{Z},\ a = bq + r$
						\item $A = \left \{d \in \mathbb{Z}^*\ \big|\ \exists k \in \mathbb{Z}^*,\ a = dk \right \}$
						\item $B = \left \{d \in \mathbb{Z}^*\ \big|\ \exists k \in \mathbb{Z}^*,\ b = dk \right \}$
						\item $R = \left \{d \in \mathbb{Z}^*\ \big|\ \exists k \in \mathbb{Z}^*,\ r = dk \right \}$
					\end{itemize}

					By definition : 

					$$
					gcd(a,b) = max(A \cap B)\ ,\ gcd(b,r) = max(B \cap R)
					$$

					Moreover, we know that :

					$$
					A \cap B = B \cap R\ \Rightarrow\ max(A \cap B) = max(B \cap R)
					$$

					Therefore, let's show that :

					\begin{equation}
					\begin{split}
						& A \cap B = B \cap R\\
						\text{(ie)}\ &\ d \in A \cap B \Leftrightarrow d \in B \cap R \\
						\text{(ie)}\ &\ d \in A \cap B \Rightarrow d \in B \cap R\ \land\\
						& d \in B \cap R \Rightarrow d \in A \cap B
					\end{split}
					\end{equation}

					Assume $d \in A \cap B$.

					\begin{equation}
					\begin{split}
						& d \in A \cap B\\
						\Leftrightarrow\ &\ d \in A\ \land\ d \in B\\
						\Leftrightarrow\ &\ d\ |\ a\ \land\ d\ |\ b\\
						\Leftrightarrow\ &\ \exists (k, k') \in \mathbb{Z}^* \times \mathbb{Z}^*, a = dk\ \land\ b = dk'
					\end{split}
					\end{equation}

					We know that :

					\begin{equation}
					\begin{split}
						a = bq + r\ \Leftrightarrow\ r & = a - bq\\
						& = dk - dk'q\ \ \text{as}\ \ a = dk\ \land\ b = dk'\\
						& = d(k - k'q)\ \ \text{factoring by d}
					\end{split}
					\end{equation}

					Because $(k, k', q) \in \mathbb{Z}^* \times \mathbb{Z}^* \times \mathbb{Z}$ then :
					\begin{equation}
					\begin{split}
						&\ \exists k'' \in \mathbb{Z}, k'' = k - k'q, r = dk''\\
						\Leftrightarrow\ &\ d\ |\ r\\
						\Leftrightarrow\ &\ d \in R
					\end{split}
					\end{equation}

					As we assumed $d \in B$ and we now know that $d \in R$ : $d \in B\ \land\ d \in R\ \Leftrightarrow\ d \in B \cap R$. We just showed that \framebox{$d \in A \cap B\ \Rightarrow\ d \in B \cap R$}.

					Assume $d \in B \cap R$.

					\begin{equation}
					\begin{split}
						& d \in B \cap R\\
						\Leftrightarrow\ &\ d \in B\ \land\ d \in R\\
						\Leftrightarrow\ &\ d\ |\ b\ \land\ d\ |\ r\\
						\Leftrightarrow\ &\ \exists (k, k') \in \mathbb{Z}^* \times \mathbb{Z}^*, b = dk\ \land\ r = dk'
					\end{split}
					\end{equation}

					We know that :

					\begin{equation}
					\begin{split}
						a & = bq + r\\
						& = dkq + dk'\ \ \text{as}\ \ b = dk\ \land\ r = dk'\\
						& = d(kq + k')\ \ \text{factoring by d}
					\end{split}
					\end{equation}

					Because $(k, k', q) \in \mathbb{Z}^* \times \mathbb{Z}^* \times \mathbb{Z}$ then :
					\begin{equation}
					\begin{split}
						&\ \exists k'' \in \mathbb{Z}, k'' = kq + k', a = dk''\\
						\Leftrightarrow\ &\ d\ |\ a\\
						\Leftrightarrow\ &\ d \in A
					\end{split}
					\end{equation}

					Now we know that $d \in A$ and we assumed that $d \in B$ : $d \in A\ \land\ d \in B\ \Leftrightarrow\ d \in A \cap B$. We just showed that \framebox{$d \in B \cap R\ \Rightarrow\ d \in A \cap B$}.\\

					\noindent To conclude, we showed that $d \in A \cap B \Rightarrow d \in B \cap R\ \ \land\ \ d \in B \cap R \Rightarrow d \in A \cap B$ which means that $A \cap B = B \cap R$. Finally $A \cap B = B \cap R \Rightarrow max(A \cap B) = max(B \cap R) \Leftrightarrow \framebox{$gcd(a,b) = gcd(b,r)$}$.
				\end{large}

				\chapter{LICENSE}

				\doclicenseThis
\end{document}
